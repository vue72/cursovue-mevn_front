import Vue from 'vue'
import Vuex from 'vuex'
import decode from 'jwt-decode'
import router from '../router'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: localStorage.getItem('token') || '',
    usuario: {}
  },
  mutations: {
    obtenerUsuario(state, token) {
      state.token = token
      if (token === '') {
        state.usuario = {}
      }
      else {
        state.usuario = decode(token)
        // router.push({name: 'Notas'})
      }
    }
  },
  actions: {
    guardarUsuario({commit}, token) {
      localStorage.setItem('token', token)

      commit('obtenerUsuario', token)
    },
    cerrarSesion({commit}) {
      localStorage.removeItem('token')

      commit('obtenerUsuario', '')
      router.push({name: 'Login'})
    },
    leerToken({commit}) {
      const token = localStorage.getItem('token')
      if (token) {
        commit('obtenerUsuario', token)
      }
      else {
        commit('obtenerUsuario', '')
      }
    }
  },
  getters: {
    estaActivo: state => !!state.token
  },
  modules: {
  }
})
